<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Http\FormRequest;

class CreateSW0102803Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'year' => 'required|max:2018|min:2025',
			'semester' => 'required|integer',
			'CGPA' => 'required|max:0.00|min:4.00',
        ];
    }
