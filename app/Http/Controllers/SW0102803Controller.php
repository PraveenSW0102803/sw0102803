<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Validation\ValidatesRequests;
use App\SW0102803;
use Illuminate\Http\Request;

class SW0102803Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sw0102803 = SW0102803::all();
		$i=1;
		return view('sw0102803.index', compact('sw0102803','i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sw0102803.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$this->validate($request,[
			'year' => 'required|digits:4|integer|min:2018|max:2025',
			'semester' => 'required|integer',
			'CGPA' => 'required|numeric|min:0.00|max:4.00',
		 ]);
		 
        $sw0102803= new SW0102803();
		$sw0102803->year = request('year');
		$sw0102803->semester = request('semester');
		$sw0102803->CGPA = request('CGPA');
		$sw0102803->save();
		return redirect('SW0102803')->with('success', 'New record has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SW0102803  $sW0102803
     * @return \Illuminate\Http\Response
     */
    public function show(SW0102803 $sW0102803)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SW0102803  $sW0102803
     * @return \Illuminate\Http\Response
     */
    public function edit(SW0102803 $sW0102803)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SW0102803  $sW0102803
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SW0102803 $sW0102803)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SW0102803  $sW0102803
     * @return \Illuminate\Http\Response
     */
    public function destroy(SW0102803 $sW0102803)
    {
        //
    }
}
