<?php

namespace App;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Database\Eloquent\Model;

class SW0102803 extends Model
{
    protected $fillable = [
		'year', 'sem', 'cgpa',
	];
}
