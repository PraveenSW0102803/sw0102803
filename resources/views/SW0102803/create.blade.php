<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
<form method="POST" action="{{ route('SW0102803.store')}}">
	@csrf
  <div class="form-group">
    <label for="year">Year</label>
    <input type="number" class="form-control" name="year" placeholder="Enter year">
  </div>
  <div class="form-group">
    <label for="semester">Semester</label>
    <input type="number" class="form-control" name="semester" placeholder="Semester">
  </div>
  <div class="form-group">
    <label for="CGPA">CGPA</label>
    <input type="text" class="form-control" name="CGPA" placeholder="CGPA">
  </div>
  <button type="submit" class="btn btn-primary">Create</button>
</form>
@if ($errors->any())
 <div class="alert-danger">
 <ul>
 @foreach($errors->all() as $error)
 <li>{{ $error }}</li>
 @endforeach
 </ul>
 </div>
@endif
</body>
</html>